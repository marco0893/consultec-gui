import {Component, NgZone, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {ClientService} from "../../services/client.service";
import {Client} from "../../services/pojos/client.model";
import {ConcessionaireService} from "../../services/concessionaire.service";
import {Concessionaire} from "../../services/pojos/concessionaire.model";
import {ToastService} from "../../services/toast.service";

@Component({
  selector: 'app-action-client',
  templateUrl: './action-client.component.html',
  styleUrls: ['./action-client.component.scss']
})
export class ActionClientComponent implements OnInit {

  clientForm: FormGroup;
  actionClient;
  clientKey = null;
  client: Client;
  txtBtnSaveOrUpdate;
  concessionaireSelected;
  concessionaires: Concessionaire[];

  constructor(private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private clientService: ClientService,
              private concessionaireService: ConcessionaireService,
              private router: Router,
              private toastrService: ToastService,
              private ngzone: NgZone
  ) {

    this.initializeClientForm();
    this.verifyAction();
  }

  isInvalid(fieldForm) {
    return this.clientForm.get(fieldForm).touched && this.clientForm.get(fieldForm).dirty && this.clientForm.get(fieldForm).invalid;
  }

  ngOnInit() {
    this.getConcessionaires();
  }

  getConcessionaires() {
    this.concessionaireService.getAllConcessionaires()
    .subscribe(
      (concessionaires: Concessionaire[]) => {
        console.log(concessionaires);
        this.concessionaires = concessionaires;
        if(concessionaires.length > 0) {
          console.log("Dentro de if");
          this.concessionaireSelected = concessionaires[0].concesionKey;
        }
        console.log('this.concessionaireSelected');
        console.log(this.concessionaireSelected)
      }, (error) => {
        this.toastrService.error('Al cargar concesionarias', 'Error');
        console.log(error)
      }
    );
  }

  verifyAction() {
    this.activatedRoute.params.subscribe((params: Params) => {
      console.log('params: ', params['action']);
      if (params['action'] === 'create') {
        this.actionClient = 'Alta';
        this.txtBtnSaveOrUpdate = 'Guardar';
      } else {
        this.actionClient = 'Modificación';
        this.clientKey = params['action'];
        this.txtBtnSaveOrUpdate = 'Actualizar';
        this.getClient();
      }
    });

  }

  initializeClientForm() {
    this.clientForm = this.formBuilder.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      age: ['', [Validators.required, Validators.pattern('^[1-9][0-9]+$')]],
      position: ['', Validators.required],
      active: ['true', Validators.required]
    });
  }

  getClient() {
    this.clientService.getClientByKey(this.clientKey)
    .subscribe(
      (client: Client) => {
        console.log(client);
        this.client = client;
        this.concessionaireSelected = client.concessionaireKey;
        console.log('Selected');
        console.log(this.concessionaireSelected);
        this.setValuesInClientForm(client);
      }, (error) => {
        this.toastrService.error('Al obtener datos del cliente', 'Error');
        console.log(error);
      }
    );
  }

  setValuesInClientForm(client: Client) {
    this.clientForm.get('name').setValue(client.name);
    this.clientForm.get('lastName').setValue(client.lastName);
    this.clientForm.get('age').setValue(client.age);
    this.clientForm.get('position').setValue(client.position);
    this.clientForm.get('active').setValue(client.active);
  }

  saveOrUpdateClient() {
    console.log('update');
    console.log(this.concessionaireSelected);
    // const formData = this.buildFormData();
    const formData = this.buildJSONForm();
    if (this.actionClient === 'Alta') {
      this.saveClient(formData);
    } else {
      this.updateClient(formData);
    }
  }

  buildFormData() {
    const formData = new FormData();
    formData.append('name', this.clientForm.value.name);
    formData.append('lastName', this.clientForm.value.lastName);
    formData.append('age', this.clientForm.value.age);
    formData.append('position', this.clientForm.value.position);
    formData.append('concessionaireKey', this.concessionaireSelected);
    // formData.append('concessionaireKey', '87db3cf2-6842-11e9-a923-1681be663d4f');
    // formData.append('active', true);
    return formData;
  }

  buildJSONForm() {
    let client = new Client(
      this.clientKey,
      this.clientForm.value.name,
      this.clientForm.value.lastName,
      this.clientForm.value.age,
      null,
      this.clientForm.value.position,
      this.concessionaireSelected,
      true
    );
    // let client = new Object

    return client;
  }

  saveClient(formData) {
    this.clientService.saveClient(formData)
    .subscribe(
      () => {
        this.toastrService.success('Cliente agregado', 'OK');
        this.back()
      }, (error) => {
        this.toastrService.error('No se pudo guardar', 'Error');
        console.log('mal');
        console.log(error);
      }
    );
  }

  updateClient(formData) {
    this.clientService.editClient(formData)
      .subscribe(
        () => {
          this.toastrService.success('Cliente actualzado', 'OK');
          this.back();
        }, (error) => {
          this.toastrService.error('No se pudo actualizar', 'Error');
          console.log(error);
        }
      );
  }

  back() {
    this.router.navigate(['/home/clientes'])
  }

}
