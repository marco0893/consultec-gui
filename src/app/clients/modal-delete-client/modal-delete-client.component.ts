import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {Subject} from "rxjs";

@Component({
  selector: 'app-modal-delete-client',
  templateUrl: './modal-delete-client.component.html',
  styleUrls: ['./modal-delete-client.component.scss']
})
export class ModalDeleteClientComponent implements OnInit {

  @Input() clientName;
  private deleteSource = new Subject<string>();
  onDelete = this.deleteSource.asObservable();

  constructor(public modal: NgbActiveModal) { }

  ngOnInit() { }

  delete() {
    this.deleteSource.next();
    this.modal.close();
  }

}
