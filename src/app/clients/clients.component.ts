import {Component, OnInit} from '@angular/core';
import {Client} from "../services/pojos/client.model";
import {Router} from "@angular/router";
import {ClientService} from "../services/client.service";
import {ConcessionaireService} from "../services/concessionaire.service";
import {Concessionaire} from "../services/pojos/concessionaire.model";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ModalDeleteClientComponent} from "./modal-delete-client/modal-delete-client.component";
import {Subscription} from "rxjs";
import {ToastService} from "../services/toast.service";

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {

  clients: Client[];
  concessionaires: Concessionaire[];
  concessionaireSelected;
  subscription: Subscription;

  constructor(private router: Router,
              private clientService: ClientService,
              private concessionairesService: ConcessionaireService,
              private modalDelete: NgbModal, private toastrService: ToastService) {
    this.getConcesionaries();
    this.getClients();
  }

  ngOnInit() {
  }

  getConcesionaries() {
    this.concessionairesService.getAllConcessionaires()
    .subscribe(
      (concessionaires: Concessionaire[]) => {
        console.log(concessionaires);
        this.concessionaires = concessionaires;
        if(concessionaires.length > 0) {
          this.concessionaireSelected = concessionaires[0].concesionKey;
        }
      }, (error) => {
        this.toastrService.error('Al cargar concesionarias', 'Error');
        console.log(error);
      }
    );
  }

  getClients() {
    this.clientService.getAllClients()
    .subscribe(
      (clients: Client[]) => {
        this.clients = clients;
        console.log(clients);
      }, (error) => {
        this.toastrService.error('Al cargar clientes', 'Error');
        console.log(error);
      }
    );
  }



  editClient(clientKey) {
    this.router.navigate([`/home/cliente/${clientKey}`]);
  }

  openModalDelete(clientKey, indexArray, nameClient) {
    const modal = this.modalDelete.open(ModalDeleteClientComponent);
    modal.componentInstance.clientName = nameClient;
    this.subscription = modal.componentInstance.onDelete.subscribe(
      () => {
        this.deleteClient(clientKey, indexArray);
      }, (error) => {
        this.toastrService.error('Al eliminar cliente', 'Error');
        console.log(error);
      }
    );
    modal.result.then(
      (result) => {
        this.subscription.unsubscribe()
      }, (result) => {
        this.subscription.unsubscribe()
      }
    );
  }

  deleteClient(clientKey, indexArray) {
    this.clientService.deleteClientByKey(clientKey)
      .subscribe(
        () => {
          this.toastrService.success('Eliminado', 'OK');
          this.clients.splice(indexArray, 1)
        }, (error) => {
          this.toastrService.error('No se pudo borrar el cliente', 'Error');
          console.log(error)
        }
      );
  }

  buildDate(dateNumber) {
    const date = new Date(dateNumber);
    const dateLastUpdate: string = date.getFullYear() + '/' + this.addPrefixZero(date.getMonth() + 1) + '/' + this.addPrefixZero(date.getDate());
    return dateLastUpdate;
  }

  addPrefixZero(needPrefix) {
    if (needPrefix <= 9) {
      return '0' + needPrefix;
    }
    return needPrefix;
  }

  searchByConcessionaire() {
    this.clientService.getAllClientsOfAConcessionaire(this.concessionaireSelected)
      .subscribe(
        (clients: Client[]) => {
          this.clients = clients;
        }, (error) => {
          this.toastrService.error('Al buscar clientes', 'Error');
          console.log(error);
        }
      );
  }

}
