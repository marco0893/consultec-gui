import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";

import {ConsultecService} from "../services/consultec.service";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CanActivateImplService implements CanActivate {

  constructor(private consultecService: ConsultecService,
              private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkLogin();
  }

  checkLogin() {
    if(this.consultecService.isAuthenticated()) {
      console.log('CanActivated Usuario logueado');
      return true;
    }

    console.log('CanActivated Usuario NO logueado');
    this.router.navigate(['/login']);
    return false;
  }

}
