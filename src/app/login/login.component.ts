import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoginService} from "../services/login.service";
import {ConsultecService} from "../services/consultec.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  logInForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private logInService: LoginService,
              private consultecService: ConsultecService,
              private router: Router) {
    this.initializeLogInForm();
  }

  ngOnInit() {
  }

  initializeLogInForm() {

    this.logInForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  buildFormData() {
    const formData = new FormData();
    formData.append('username', this.logInForm.value.username);
    formData.append('password', this.logInForm.value.password);
    return formData
  }

  logIn() {
    const formData: FormData = this.buildFormData();
    // this.logInService.login(formData)
    // .subscribe(
    //   () => {
        this.consultecService.setUserName(this.logInForm.value.username);
        this.router.navigate(['/home/clientes']);
      // }, (error) => {
      //   console.log(error);
      // }
    // );
  }

  isInvalid(field) {
    return this.logInForm.get(field).touched && this.logInForm.get(field).dirty && this.logInForm.get(field).invalid;
  }

}
