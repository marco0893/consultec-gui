import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {LoginComponent} from "./login/login.component";
import {ClientsComponent} from "./clients/clients.component";
import {CanActivateImplService} from "./auth/can-activate-impl.service";
import {ActionClientComponent} from "./clients/action-client/action-client.component";
import {HomeComponent} from "./home/home.component";

const routes: Routes = [
  {path: '', redirectTo: '/home/clientes', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'home', component: HomeComponent,
    // canActivate: [CanActivateImplService],
    children: [
      {path: 'clientes', component: ClientsComponent},
      {path: 'cliente/:action', component: ActionClientComponent},
    ]
  },
  // {path: 'clientes/:action', component: ActionClientComponent},
  {path: '**', component: ClientsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})

export class AppRountingModule {

}
