import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ConsultecService} from "../services/consultec.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router,
              private consultecService: ConsultecService) { }

  ngOnInit() {
  }

  goClients() {
    this.router.navigate(['/home/clientes'])
  }

  addClient() {
    this.router.navigate(['/home/cliente/create'])
  }

  logOut() {
    this.consultecService.logOut();
    this.router.navigate(['/login']);
  }
}
