import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRountingModule} from "./app-rounting.module";
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {HttpClientModule} from "@angular/common/http";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {ClientsComponent} from './clients/clients.component';
import {LoginService} from "./services/login.service";
import {ConsultecService} from "./services/consultec.service";
import {ClientService} from "./services/client.service";
import {UrlProviderService} from "./services/url-provider.service";
import { ActionClientComponent } from './clients/action-client/action-client.component';
import { ModalDeleteClientComponent } from './clients/modal-delete-client/modal-delete-client.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import {ConcessionaireService} from "./services/concessionaire.service";
import {ToastService} from "./services/toast.service";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ClientsComponent,
    ActionClientComponent,
    ModalDeleteClientComponent,
    HeaderComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRountingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [
    LoginService,
    ConsultecService,
    ConcessionaireService,
    ClientService,
    ToastService,
    UrlProviderService
  ],
  bootstrap: [AppComponent],
  entryComponents: [ModalDeleteClientComponent]
})
export class AppModule {
}
