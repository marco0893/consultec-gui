import {Injectable} from '@angular/core';
import {ConsultecService} from "./consultec.service";

@Injectable({
  providedIn: 'root'
})
export class UrlProviderService {

  constructor(private consultecService: ConsultecService) {
  }

  getClientByKeyUrl(clientKey) {
    return this.consultecService.getDomainUrl() + `/client/${clientKey}`;
  }

  getAllCleintsUrl() {
    return this.consultecService.getDomainUrl() + '/clients';
  }

  saveClientUrl() {
    return this.consultecService.getDomainUrl() + '/client';
  }

  editClientByKeyUrl() {
    return this.consultecService.getDomainUrl() + '/client';
  }

  deleteClientByKeyUrl(clientKey) {
    return this.consultecService.getDomainUrl() + `/client/${clientKey}`;
  }

  getAllClientsOfAConcessionaireUrl(concessionaireKey) {
    return this.consultecService.getDomainUrl() + `/concessionaire/clients/${concessionaireKey}`;
  }

  /**/
  getAllConcessionairesUrl() {
    return this.consultecService.getDomainUrl() + '/concessionaires';
  }
}
