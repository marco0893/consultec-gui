export class Client {
  constructor(
    public clientKey: string,
    public name: string,
    public lastName: string,
    public age: number,
    public lastUpdate: number,
    public position: string,
    public concessionaireKey: string,
    public active: boolean
  ) { }
}
