import {Injectable} from '@angular/core';
import {UrlProviderService} from "./url-provider.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Concessionaire} from "./pojos/concessionaire.model";
import {ConsultecService} from "./consultec.service";

@Injectable({
  providedIn: 'root'
})
export class ConcessionaireService {

  constructor(private http: HttpClient,
              private urlProviderService: UrlProviderService,
              private consultecService: ConsultecService) {
  }

  getAllConcessionaires(): Observable<Concessionaire[]> {
    const url = this.urlProviderService.getAllConcessionairesUrl();
    // const headers = new HttpHeaders({'Content-Type': 'application/json', 'Authorization': `${this.consultecService.getTokebUser()}`});
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.get<Concessionaire[]>(url, {headers});
  }

}
