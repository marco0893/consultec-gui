import { Injectable } from '@angular/core';
declare var toastr;

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor() {
    toastr.options = {
      'closeButton': true,
      'timeOut': '2000',
      'extendedTimeOut': '2000',
      'progressBar': false,
      'positionClass': 'toast-top-right'
    }
  }

  success(message: string, title?: string ): void {
    toastr.success(message, title);
  }

  error(message: string, title?: string ): void {
    toastr.error(message, title);
  }
}
