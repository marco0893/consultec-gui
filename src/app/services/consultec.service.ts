import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ConsultecService {

  isLoggin= false;
  userName: string;

  constructor() { }

  setUserName(userName: string) {
    this.isLoggin = true;
    this.userName = userName;
  }

  isAuthenticated() {
    return this.isLoggin;
  }

  logOut() {
    this.isLoggin = false;
  }

  getDomainUrl() {
    return environment.api;
  }

  getTokebUser() {
    return localStorage.getItem('token');
  }
}
