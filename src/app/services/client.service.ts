import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Client} from "./pojos/client.model";
import {UrlProviderService} from "./url-provider.service";
import {ConsultecService} from "./consultec.service";

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient,
              private urlProviderService: UrlProviderService,
              private consultecService: ConsultecService) { }

  getClientByKey(clientKey): Observable<Client> {
    const url = this.urlProviderService.getClientByKeyUrl(clientKey);
    // const headers = new HttpHeaders({'Content-Type': 'application/json', 'Authorization': `${this.consultecService.getTokebUser()}`});
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.get<Client>(url, {headers});
  }

  getAllClients(): Observable<Client[]> {
    const url = this.urlProviderService.getAllCleintsUrl();
    // const headers = new HttpHeaders({'Content-Type': 'application/json', 'Authorization': `${this.consultecService.getTokebUser()}`});
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.get<Client[]>(url, {headers});
  }

  saveClient(formData): Observable<Client> {
    const url = this.urlProviderService.saveClientUrl();
    // const headers = new HttpHeaders({'Content-Type': 'application/json', 'Authorization': `${this.consultecService.getTokebUser()}`});
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post<Client>(url, formData, {headers});
  }

  editClient(formdata): Observable<any> {
    const url = this.urlProviderService.editClientByKeyUrl();
    // const headers = new HttpHeaders({'Content-Type': 'application/json', 'Authorization': `${this.consultecService.getTokebUser()}`});
    const headers = new HttpHeaders({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'});
    return this.http.patch<any>(url, formdata, {headers});
  }

  deleteClientByKey(clientKey): Observable<Client> {
    const url = this.urlProviderService.deleteClientByKeyUrl(clientKey);
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    // const headers = new HttpHeaders({'Content-Type': 'application/json', 'Authorization': `${this.consultecService.getTokebUser()}`});
    return this.http.delete<Client>(url, {headers});
  }

  getAllClientsOfAConcessionaire(concessionaireKey: string): Observable<Client[]> {
    const url = this.urlProviderService.getAllClientsOfAConcessionaireUrl(concessionaireKey);
    const headers = new HttpHeaders({'Content-Type': 'application/json', 'Authorization': `${this.consultecService.getTokebUser()}`});
    return this.http.get<Client[]>(url, {headers})
  }
}
