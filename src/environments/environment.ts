export const environment = {
  production: false,
  hmr: false,
  api: 'http://localhost:8080/consultec/api'
};
