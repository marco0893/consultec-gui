export const environment = {
  production: false,
  hmr: true,
  api: 'http://localhost:8080/consultec/api'
};
